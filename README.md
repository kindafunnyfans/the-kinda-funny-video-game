# README #

Hi there and welcome to development of the fan made Kinda Funny game! This git page will provide you the latest version of all the code and game files for you to work on if you'd like to contribute. We are always looking for more people to help out :D.

If you are

### What is this repository for? ###

* The Kinda Funny Video Game (Whatever shape that may end up taking)
* The game is totally fan made and is not for any profit. It's just for fun!
* Making sure the Kinda Funny fan base can make a Vita game :P

### How do I get set up? ###

* You want to pull the files from the "master branch", if you have never used git before then I'd recommend a tutorial like this one: https://www.youtube.com/watch?v=0fKg7e37bQE. 
* The master branch contains the whole code of the game. Each person contributing to the project are welcome to create their own project folder in the root of the master branch if they like so that they can put any ideas and additions up there. Artists are also encouraged to upload art to the branch so we can all pool resources!  :)
* Depending on what engine we end up using, there will be more information here on configuration.

### Contribution guidelines ###

* Pull from master and create your own branch if you are submitting stuff. That way we can check pull requests and make sure that submissions will work properly, when your work is verified we will merge it into the master branch. Feel free to then continue work on your branch. :)
* Don't get offended if code submitted for the game gets declined a couple times. It's going to happen to all of us, we all want the game to be as polished as possible ok :D
* Please join the Facebook page for development so we can all communicate and discuss development!

### Who do I talk to? ###

* Any admin :D. We basically all want to help so if you are stuck on something PLEASE let us know. Happy to help.

### Thank for for contributing :) ###