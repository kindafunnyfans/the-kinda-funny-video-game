﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;

public class AudioManager : MonoBehaviour
{

    public bool mute = false;
    public float defaultVolume = 1.0f;

    [SerializeField]
    private AudioTrack currentTrack;
    [SerializeField]
    private AudioSource currentSource;
    [SerializeField]
    private AudioSource source1;

    public AudioTrack[] music;

    //private AudioClip currentAudioClip = null;
    private int timeSamples;

    private static AudioManager _instance;

    public static AudioManager instance

    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<AudioManager>();

                //If there is an instance available:
                if (_instance != null)
                {
                    //Tell unity not to destroy this object when loading a new scene!
                    DontDestroyOnLoad(_instance.gameObject);
                }
            }

            return _instance;
        }
    }

    //All this script does is makes sure that the gameObject with the music attached to it will carry over to all scenes and continue to play seamlessly.

    //Best method for this is to use a scene that is only loaded once and can never been loaded again through the game.
    //In this case, the scene Loader is loaded first then brings us to the Menu once we established that the music manager understands that it shouldn't be deleted.
    //If an object with a dontdestroyonload is loaded more than once, you'll get duplicates.

    void Awake()
    {

        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            if (this != _instance)
            {
                Destroy(this.gameObject);
            }
        }
    }

    void Start()
    {

        // Get audio source, store it in source variable.
        // Ignore listener volume (this is just music).
        source1.ignoreListenerVolume = true;
        // Ignore listener effects.
        source1.bypassListenerEffects = true;
        // Set volume.
        source1.volume = defaultVolume;

        currentSource = source1;

        // Set the game music to the music of the current scene.
        changeMusic(SceneManager.GetActiveScene().name);

    }

    void Update()
    {

        // Switch between mute and not mute with keypress M.
        // This is temporary.
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (mute)
            {
                Mute = false;
            }
            else
            {
                Mute = true;
            }

            AudioSource[] allsources = UnityEngine.Object.FindObjectsOfType(typeof(AudioSource)) as AudioSource[];

            foreach (AudioSource isource in allsources)
            {
                if (mute) { isource.mute = true; }
                else { isource.mute = false; }
            }
        }


    }

    void OnLevelWasLoaded()
    {

        changeMusic(SceneManager.GetActiveScene().name);

        // Get all audiosources in the scene.
        AudioSource[] allsources = UnityEngine.Object.FindObjectsOfType(typeof(AudioSource)) as AudioSource[];

        // For every audiosource in the scene:
        foreach (AudioSource isource in allsources)
        {
            // If muted, then turn off all audio sources.
            if (mute) { isource.mute = true; }
            // If not then unmute all audio sources.
            else { isource.mute = false; }
        }

    }

    public void changeMusic(string levelName)
    {
        currentSource.volume = defaultVolume;

        // If the counter goes above 0, the next block of code
        // won't execute.
        int counter = 0;

        // Search for the string levelname inside
        // every audio track name on file.
        foreach (AudioTrack track in music)
        {
            if (track.Name == levelName)
            {
                currentTrack = track;
                counter++;
            }
        }

        // Search for the string levelname inside
        // every audio track's listed scene names on file.
        if (counter == 0)
        {
            foreach (AudioTrack track in music)
            {
                for (int i = 0; i < track.SceneNames.Length; i++)
                {
                    if (levelName == track.SceneNames[i])
                    {
                        currentTrack = track;
                        counter++;
                    }
                }
            }
        }

        // If counter is still zero, remove any audio clip that
        // is currently being used in the audio source.
        if (counter == 0)
        {
            currentTrack = null;
        }

        // Play the audio
        if (!currentSource.isPlaying)
        {
            currentSource.timeSamples = 0;
        }

        if (currentTrack.Clip == currentSource.clip)
        {
            //resume ();
        }
        else
        {
            currentSource.clip = currentTrack.Clip;
        }

        if (currentSource.isPlaying == false)
        {
            currentSource.Play();
        }

        SaveTimeSamples();

    }

    public AudioSource CurrentSource
    {
        get { return currentSource; }
    }

    public bool Mute
    {
        get { return mute; }
        set
        {
            mute = value;
            AudioSource[] allsources = UnityEngine.Object.FindObjectsOfType(typeof(AudioSource)) as AudioSource[];

            foreach (AudioSource isource in allsources)
            {
                if (mute) { isource.mute = true; }
                else { isource.mute = false; }
            }
        }
    }

    public bool IsPlaying
    {
        get { return currentSource.isPlaying; }
    }

    public void SaveTimeSamples()
    {
        timeSamples = currentSource.timeSamples;
    }

    public void Stop()
    {
        timeSamples = currentSource.timeSamples;
        currentSource.Stop();
    }

    public void PauseMusic()
    {
        timeSamples = currentSource.timeSamples;
        currentSource.Pause();
    }

    public void Resume()
    {
        currentSource.timeSamples = timeSamples;
        currentSource.Play();
    }

    public void PlayMusic()
    {
        currentSource.Play();
    }

}

[Serializable]
public class AudioTrack
{
    [SerializeField]
    private string trackName;
    [SerializeField]
    private string artistName;
    [SerializeField]
    private AudioClip clip;
    [SerializeField]
    private string[] sceneNames;

    public string Name
    {
        get { return trackName; }
        set { trackName = value; }
    }

    public string Artist
    {
        get { return artistName; }
        set { artistName = value; }
    }

    public AudioClip Clip
    {
        get { return clip; }
        set { clip = value; }
    }

    public string[] SceneNames
    {
        get { return sceneNames; }
        set { sceneNames = value; }
    }


};