﻿using UnityEngine;
using System.Collections;

/*
 * 		Game Manager class
 * 		Tom Taylor 01/05/2016
 * 
 * 		This Game Manager class holds all the variables that need to carry across different scenes,
 *      whether you find it useful or not is up to you but there isn't much of a downside of having one.
 * 
 * 		v0.1 | 01/05/2016 | Created the Game Manager class. It is currently empty.
 * 
 * 										*/

public class GameManager : MonoBehaviour {

	// Creates a static instance that should exist the entire gameplay session.
	private static GameManager _instance;

	// The code that makes up the instance of this class.
	public static GameManager instance
	{
		// The getter for the instance.
		get
		{
			// If there is currently no instance,
			if(_instance == null)
			{
				// _instance becomes the current instance of the Game Manager.
				_instance = GameObject.FindObjectOfType<GameManager>();

				// If this object is now the current instance,
				if ( _instance != null ) {
					//Tell unity not to destroy this object when loading a new scene!
					DontDestroyOnLoad(_instance.gameObject);
				}
			}

			// Return this instance of the Game Manager script.
			return _instance;
		}
	}

	// This code starts when the script is first ever activated.
	void Start() {
		// If there is no instance of the Game Manager in the scene,
		if(_instance == null)
		{
			// This is the new instance.
			_instance = this;
			// And don't destroy this object when changing scenes.
			DontDestroyOnLoad(this);
		}
		// If there is already a Game Manager in this scene,
		else
		{
			// If this is not the current Game Manager,
			if(this != _instance){
				// Destroy this whole Game Object.
				Destroy(this.gameObject);
			}
		}

	}
}
