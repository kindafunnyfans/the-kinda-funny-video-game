﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

/*
 * 		Change Scene After Time class
 * 		Tom Taylor 01/05/2016
 * 
 * 		This script, when attached to an object will force the scene
 * 		to change after a given number of seconds, depending on input
 * 		to the script.
 * 
 * 		v1.0 | 01/05/2016 | Created the class. It works perfectly.
 * 
 * 										*/

public class ChangeSceneAfterTime : MonoBehaviour {

	// Use a fade out transition, or just cut to the new scene. (Cut by default)
	[SerializeField]
	private bool transitionOn = false;

	// The float that stores the time until the scene should change, in seconds.
	[SerializeField]
	private float timeUntilSceneChange;

	// The string that stores the name of the scene that should load.
	[SerializeField]
	private string sceneToLoad;

	// The designer has the choice to import a transition to use when changing scene.
	// These are prefabs that can be found in the "Prefabs" folder. For more information,
	// Please look at a Transition prefab and the variables it stores.
	[SerializeField]
	private GameObject transition;

	// This code starts when the script is first ever activated.
	void Start() {
		// Start the change scene routine.
		StartCoroutine(ChangeScene());
	}

	// This is IEnumerator that changes the scene.
	IEnumerator ChangeScene() {

		switch ( transitionOn ) {
			case true:
				// Wait exactly the amount of time specified in "timeUntilSceneChange"
				//yield return new WaitForSeconds(timeUntilSceneChange - time);
				// Load the requested scene from "sceneToLoad"
				SceneManager.LoadScene(sceneToLoad);
			break;
			case false:
				// Wait exactly the amount of time specified in "timeUntilSceneChange"
				yield return new WaitForSeconds(timeUntilSceneChange);
				// Load the requested scene from "sceneToLoad"
				SceneManager.LoadScene(sceneToLoad);
			break;
		}
		// Wait exactly the amount of time specified in "timeUntilSceneChange"
		yield return new WaitForSeconds(timeUntilSceneChange);
		// Load the requested scene from "sceneToLoad"
		SceneManager.LoadScene(sceneToLoad);
	}
}
